import React from 'react';
import './styles/style.css';
import Keypad from './components/Keypad';
import Display from './components/Display';
import History from './components/History';
import * as Math from 'mathjs';

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      output: '',
      result: '',
      history: [],
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(event) {
    const target = event.target.innerText;
    try {
      if(isNaN(target) === false) {
        this.setState((prevState) => {
          return {
            output: prevState.output + target,
            result: '',
          };
        });
      } else if (['+', '-', '/', '*', '%'].includes(target)) {
        this.setState((prevState) => {
          return {
            output: `${prevState.output} ${target} `,
          };
        });
      } else if(target === '=') {
        let calculate = '';
        this.setState((prevState) => {
          calculate = Math.evaluate(prevState.output);
          this.state.history.push(`${prevState.output} = ${calculate} |`);
          return {
            result: calculate,
            output: '',
          };
        });
      } else if(target === '.') {
        this.setState(prevState => {
          return {
            output: prevState.output + target,
            result: '',
          }
        }) 
      } else if(target === 'C' || target === 'CE') {
        this.setState({ output: '', result: ''})
      } else if('↻') {
        this.setState({result : this.state.history});
        setTimeout(() =>{
          alert('Press C to clear history');
        },1000)
      }
    } catch(e) {
        alert('Invalid operation');
    }
}

  render() {
    return (
      <div >
        <div className="main-container">
        <Display output={this.state.output} result={ this.state.result } />
        <History handleClick={this.handleClick}/>
        <Keypad handleClick={this.handleClick} />
        </div>
      </div>
    );
  }
}

export default App;
