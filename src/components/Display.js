import React from 'react';
class Display extends React.Component {
  render() {
    return (
      <div className="display">
        <div>{this.props.output}</div>
        <div>{this.props.result}</div>
        <div className='display-big'>{this.props.history}</div>
      </div>
    );
  }
}

export default Display;
