import React from 'react';
class History extends React.Component {
  render() {
    return (
      <div className="history">
        <button className='history-btn' onClick={this.props.handleClick}>↻</button>
        </div>
    );
  }
}

export default History;
