import React from "react";
import Header from './Header';
import {Link} from 'react-router-dom';

function Nav() {
    return (
        <nav>
            <Header/>
            <div>
            <Link style={{ textDecoration: 'none' }} to='/'>
            <div>Home</div>
            </Link>
            <Link style={{ textDecoration: 'none' }} to='/about'>
            <div>About</div>
            </Link>
            <Link style={{ textDecoration: 'none' }} to='/contact'>
            <div>Contact</div>
            </Link>
            </div>
      </nav>
    );
}

export default Nav;
