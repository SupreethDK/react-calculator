import React from 'react';

const buttonList = ['CE', 'C', '◀', '/', '7', '8', '9', '*', '4', '5', '6', '-', '1', '2', '3', '+', '%', '0', '.', '='];
class Keypad extends React.Component {
  render() {
    return (
      <div className="calculator">
          {buttonList.map((button) => <button key={button} onClick={this.props.handleClick}>{button}</button>)}
      </div>
    );
  }
}

export default Keypad;
