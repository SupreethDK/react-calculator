import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Nav from './components/Nav';
import About from './components/About';
import Contact from './components/Contact';
import {
    BrowserRouter as Router,
    Switch,
    Route,
  } from "react-router-dom";

ReactDOM.render(
    <Router>
        <Nav/>  
        <Switch>
            <Route path='/' exact component={App}></Route>
            <Route path="/about" exact component={About} /> 
            <Route path="/contact" exact component={Contact} /> 
        </Switch>
    </Router>
,

document.getElementById('root'));
